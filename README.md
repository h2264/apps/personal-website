# personal-website

Personal website under construction.

# ToDo
- [x] CI/CD OCI image builder/publisher
- [ ] Helm chart
- [ ] CI/CD K8s deployer
- [ ] Decouple content from application
- [ ] Decouple CV content from astro template
- [ ] Update README