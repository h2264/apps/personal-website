---
title: "Blog deployment"
description: "Initialisation of blog front end artefacts and development of scaffolding for automated deployments"
pubDate: "2023/04/23"
heroImage: "/images/placeholder.jpeg "
---

## Motivation
To initialise a personal site that includes functions required for technical blogs and CV entry. The source code can be located at https://gitlab.com/h2264/apps/personal-website

## Components
| Component Name | Comment | Reference |
| --- | --- | --- |
| Astro.js | Web Framework | https://astro.build/ |
| cicd-makefiles | Automation Library | https://gitlab.com/h2264/libraries/cicd-makefiles |
| Docker | Containerisation Technology | https://www.docker.com/ |
| Helm | Kubernetes package manager | https://helm.sh/ |
| Jenkins | CI/CD Platform | https://www.jenkins.io/ |
| Kubernetes | Container orchestrator | https://kubernetes.io/ |
||||

## Usage
The frequently updated entities includes the following:

- Assets
- Blog
- CV
- Projects

### Assets
This contains static files localised to the node application's filesystem. The are common media files. i.e. images, music, gifs, etc.

### Blog
Contains technical blog posts.

### CV
Curriculum Vitae. This is a live file containing a summary of my career, qualifications, and education.

### Project
Contains entries involving development of an application or a solution.