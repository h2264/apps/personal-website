import { defineConfig } from 'astro/config';
import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';

import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  site: 'https://astro-modern-personal-website.netlify.app',
  integrations: [mdx(), sitemap(), tailwind()],
  // Example: Use the function syntax to customize based on command
  server: (command) => ({ host: command === 'dev' ? "0.0.0.0" : "0.0.0.0" })
});