# include base makefile
include .make/base.mk

# include makefiles to be inherited
include .make/oci.mk

# include makefiles to be inherited
include .make/dev.mk

# include repo specific override residing in current repository
-include Override.mk

# include workstation specific targets
-include WorkstationTargets.mk